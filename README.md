# Watchovr MVP

CloudFlare Workers + Typescript + Webpack boilerplate template

Reference [Cloudflare Worker TypeScript Router](https://github.com/drewdecarme/cloudflare-workers-ts-router/blob/master/README.md) for a full example.

## Initial Setup

1. Clone & `npm install`
1. Rename `example.env` to `.env` and set values

## Development

1. Run `package.json` script to start _a_ development version of the worker
   > `npm run start`  
Builds the development webpack bundle and then opens up the Cloudflare worker sandbox environment where you can test your requests live.

1. Edit files in `src/`

## Deployment

Workers can be deployed via 2 options depending on the use case.

1. [Wrangler.md](https://github.com/drewdecarme/cloudflare-workers-ts-router/blob/master/docs/deployment-wrangler.md): Deploy to `workers.dev` test environment
    > `npm run publish:dev`

1. [Serverless.md](https://github.com/drewdecarme/cloudflare-workers-ts-router/blob/master/docs/deployment-serverless.md): Deploy to workers `$CLOUDFLARE_DOMAIN` endpoint  
    > `npm run serverless:deploy`

Click on the options to view the documentation around deployment.

### Initial Setup (For reference only)

1. > `serverless create --template cloudflare-workers --path watchovr-mvp --name=watchovr-mvp`
1. Add Typescript & Webpack support based on [Cloudflare Worker TypeScript Router](https://github.com/drewdecarme/cloudflare-workers-ts-router/blob/master/README.md)
1. This repo going forward is a better starter-template.
